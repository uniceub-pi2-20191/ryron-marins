import React, {Component} from 'react';
import {View, Image, StyleSheet, TextInput, Button} from 'react-native';
import LoginForm from '../login-form/loginForm';


export default class Login extends Component {
	
	render() {
		return (
			<View style={styles.container}>
				<View style={styles.inner}>
					<LoginForm/>
				</View>
			</View>
		);
	}
}
const styles = StyleSheet.create({
	container: {
	    borderRadius: 4,
		borderWidth: 0.5,
	    backgroundColor: '#0D6969', //0D6969
	    height: '100%'

	},
	inner: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'space-around',
	},

	// não conseguir colocar as duas dimensões em "%", visto que, por algum motivo, a imagem acabava sendo cortada em algum ponto.
	image: {
	    width: 250,
		height: "58%"
  	}
});


