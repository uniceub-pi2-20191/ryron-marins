import React, { Component } from 'react';
import { AppRegistry, Image, Text, View } from 'react-native';

export default class Bananas extends Component {
  render() {
    let pic = {
      uri: 'https://ichef.bbci.co.uk/news/660/cpsprodpb/DA0F/production/_99832855_gettyimages-881872502-1.jpg'  
    };
    let pic2 = {
      uri: 'http://animais.culturamix.com/blog/wp-content/gallery/macaco-2-1/Macaco-3.jpg'
    };

    return (
      <View style={{flex: 1, flexDirection: "column"}}>
        <View style={{ alignItems: "center"}}>
          <Image source={pic} style={{width: 200, height: 100, top: 100, left: 1}}/>
        </View>
        
        <View style={{ alignItems: "center"}}>
          <Text style={{width: 200, height: 100, top: 100, left: 75}}>Macacos</Text>
        </View>

        <View style={{ alignItems: "center"}}>
          <Image source={pic2} style={{width: 200, height: 100, top: 100, left: 1}}/>
        </View>
        
        <View style={{ alignItems: "center"}}>
          <Text style={{width: 200, height: 100, top: 100, left: 75}}>Macacos</Text>
      
        </View>
      </View>
    );
  }
}